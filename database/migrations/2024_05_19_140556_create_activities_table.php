<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->id();
            $table->string('jenis')->default('cuci');
            $table->unsignedBigInteger('id_bus');
            $table->unsignedBigInteger('id_cabang');
            $table->string('process')->default('');
            $table->string('images')->default('');
            $table->unsignedBigInteger('id_user');
            $table->foreign('id_bus')->references('id')->on('master_buses');
            $table->foreign('id_cabang')->references('id')->on('master_cabangs');
            $table->foreign('id_user')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
