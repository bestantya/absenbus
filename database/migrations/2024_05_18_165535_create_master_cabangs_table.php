<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterCabangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('master_cabangs', function (Blueprint $table) {
            $table->id();
            $table->string('nama_cabang');
            $table->string('alamat_cabang');
            $table->string('lat');
            $table->string('lang');
            $table->string('images');
            $table->string('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_cabangs');
    }
}
