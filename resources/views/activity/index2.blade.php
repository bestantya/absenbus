@extends('layouts.app')

@section('content')
<div class="container">
    <a href="{{ route('activity.create') }}" class="btn btn-primary mb-3">Create Activity</a>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Jenis</th>
                    <th>Bus</th>
                    <th>Proses</th>
                    <th>Foto</th>
                    <th>Tanggal</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($activities as $activity)
                    <tr>
                        <td>{{ $activity->jenis }}</td>
                        <td>{{ $activity->id_bus }}</td>
                        <td>{{ $activity->process }}</td>
                        <td>{{ $activity->images }}</td>
                        <td>{{ $activity->created_at }}</td>
                        <td>
                            <a href="{{ route('activity.show', $activity->id) }}" class="btn btn-info">View</a>
                            <a href="{{ route('activity.edit', $activity->id) }}" class="btn btn-warning">Edit</a>
                            <form action="{{ route('activity.destroy', $activity->id) }}" method="POST"
                                style="display:inline;">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger"
                                    onclick="return confirm('Are you sure?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection