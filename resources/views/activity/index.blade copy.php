<!DOCTYPE html>
<html>

<head>
    <title>Laravel AJAX CRUD</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>
    <div class="container mt-5">
        <h2>Laravel 7 AJAX CRUD</h2>
        <button class="btn btn-success" id="create-new-article">Add Article</button>
        <br><br>
        <div class="alert alert-success d-none" id="success-message"></div>
        <table class="table table-bordered" id="articles-table">
            <thead>
                <tr>
                    <th>Jenis Kegiatan</th>
                    <th>Bus</th>
                    <th>Pool</th>
                    <th>Proses</th>
                    <th>Foto</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <div id="pagination"></div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="article-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="article-modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form id="article-form" name="article-form" class="form-horizontal">
                        <input type="hidden" name="activity_id" id="activity-id">

                        <div class="form-group">
                            <label>Jenis</label>
                            <select class="form-control" required id="jenis" name="jenis">
                                <option value="cleaning_bus">Cuci Bus </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Bus</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="id_bus" name="id_bus" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Cabang</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="id_cabang" name="id_cabang" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Proses</label>
                            <select class="form-control" required id="process" name="process">
                                <option value="sebelum"> Sebelum </option>
                                <option value="berlangsung"> Sedang Berlangsung </option>
                                <option value="setelah"> Setelah </option>
                            </select>
                        </div>


                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary" id="save-btn">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            loadArticles();

            function loadArticles(page = 1) {
                $.ajax({
                    url: "/activities?page=" + page,
                    type: "GET",
                    dataType: 'json',
                    success: function (data) {
                        var rows = '';
                        $.each(data.data, function (key, article) {
                            rows += '<tr>';
                            rows += '<td>' + article.jenis + '</td>';
                            rows += '<td>' + article.id_bus + '</td>';
                            rows += '<td>' + article.id_cabang + '</td>';
                            rows += '<td>' + article.proses + '</td>';
                            rows += '<td>' + article.image + '</td>';
                            rows += '<td>' + article.created_at + '</td>';
                            rows += '<td>';
                            rows += '<button data-id="' + article.id + '" class="btn btn-warning edit-article">Edit</button> ';
                            rows += '<button data-id="' + article.id + '" class="btn btn-danger delete-article">Delete</button>';
                            rows += '</td>';
                            rows += '</tr>';
                        });
                        $('#articles-table tbody').html(rows);
                        $('#pagination').html(data.pagination);
                    }
                });
            }

            $('body').on('click', '#create-new-article', function () {
                $('#save-btn').val("create-article");
                $('#activity-id').val('');
                $('#article-form').trigger("reset");
                $('#article-modal-title').html("Tambah Kegiatan");
                $('#article-modal').modal('show');
            });

            $('body').on('click', '.edit-article', function () {
                var id = $(this).data('id');
                $.get('activities/' + id, function (data) {
                    $('#article-modal-title').html("Ubah Kegiatan");
                    $('#save-btn').val("edit-article");
                    $('#article-modal').modal('show');
                    $('#activity-id').val(data.id);
                    $('#title').val(data.jenis);
                    $('#body').val(data.id_bus);
                    $('#body').val(data.proses);
                });
            });

            $('body').on('submit', '#article-form', function (e) {
                e.preventDefault();
                var actionType = $('#save-btn').val();
                var id = $('#activity-id').val();
                var method = actionType == "edit-article" ? "PUT" : "POST";
                var url = actionType == "edit-article" ? 'activities/' + id : 'activities';

                $.ajax({
                    type: method,
                    url: url,
                    data: $('#article-form').serialize(),
                    success: function (data) {
                        $('#article-form').trigger("reset");
                        $('#article-modal').modal('hide');
                        $('#success-message').html(data.message).removeClass('d-none');
                        loadArticles();
                    }
                });
            });

            $('body').on('click', '.delete-article', function () {
                var id = $(this).data('id');
                if (confirm("Are you sure want to delete this article?")) {
                    $.ajax({
                        type: "DELETE",
                        url: 'activities/' + id,
                        success: function (data) {
                            $('#success-message').html(data.message).removeClass('d-none');
                            loadArticles();
                        }
                    });
                }
            });

            $('body').on('click', '.pagination a', function (e) {
                e.preventDefault();
                var page = $(this).attr('href').split('page=')[1];
                loadArticles(page);
            });
        });
    </script>
</body>

</html>