@extends('layouts.app')
@section('content')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet"
    href="{{ asset('vendor/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<style>
    .select2-container--default .select2-selection--single {
        height: calc(2.25rem + 2px);
    }
</style>
<div>
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">{{ __('Activity') }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Activity</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive"><button class="btn btn-success" id="create-new-article">Tambah
                            Kegiatan</button>
                        <div class="alert alert-success d-none mt-2" id="success-message"></div>
                        <table class="table table-bordered mt-2" id="articles-table">
                            <thead>
                                <tr>
                                    <th>Bus</th>
                                    <th>Pool</th>
                                    <th>Proses</th>
                                    <th>Foto</th>
                                    <th>Pembuat</th>
                                    <th>Tanggal Buat</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div id="pagination"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="article-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="article-modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="article-form" name="article-form" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" name="activity_id" id="activity-id">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Minimal</label>
                        <div class="col-sm-12">
                            <select class="form-control select2" style="width: 100%;" required id="id_bus"
                                name="id_bus">
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Cabang</label>
                        <div class="col-sm-12">
                            <select class="form-control" required id="id_cabang" name="id_cabang">
                                @foreach($cabang as $key)
                                    <option value="{{$key->id}}">{{$key->nama_cabang}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Proses</label>
                        <div class="col-sm-12">
                            <select class="form-control" required id="process" name="process">
                                <option value="sebelum"> Sebelum </option>
                                <option value="berlangsung"> Sedang Berlangsung </option>
                                <option value="setelah"> Setelah </option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="image" class="col-sm-2 control-label">Ambil Gambar</label>
                        <div class="col-sm-12">
                            <input type="file" id="images" name="images" accept="image/*" capture="camera"
                                class="form-control">
                        </div>
                    </div>
                    <div id="uploaded-image" class="mt-5 mb-3">
                        <img id="image-preview" src="#" alt="Your Image" class="img-fluid d-none" />
                    </div>

                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary" id="save-btn">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- 
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->
<script src="{{ asset('vendor/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('vendor/adminlte/plugins/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript">
    function jenisBus(param) {
        switch (param) {
            case 0:
                param = "Small"
                break;
            case 1:
                param = "Medium"
                break;
            case 2:
                param = "Big"
                break;
            default:
                param = "Small"
                break;
        }
        return param;
    }

    // $('.select2').select2();
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    });

    $('.select2').select2({
        ajax: {
            url: '{{ route('activity.getBus') }}',
            data: function (params) {
                var query = {
                    term: params.term
                }
                return query;
            },

            processResults: function (data) {

                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.plat + " (" + jenisBus(item.jenis) + ")",
                            id: item.id
                        }
                    })
                }
            }
        }
    });


    // $('#autocomplete').autocomplete({
    //     source: function (request, response) {
    //         $.ajax({
    //             url: "{{ route('activity.getBus') }}",
    //             data: {
    //                 term: request.term
    //             },
    //             dataType: "json",
    //             success: function (data) {
    //                 var items = $.map(data, function (item) {
    //                     return {
    //                         label: item.plat + " (" + jenisBus(item.jenis) + ")",
    //                         value: item.id
    //                     };
    //                 });
    //                 response(items);
    //             }
    //         });
    //     },
    //     minLength: 2,
    // });

    $(document).ready(function () {


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        loadArticles();
        var ass = "{{ asset('/') }}img/";
        // Preview image before upload
        $('#images').change(function () {
            let reader = new FileReader();
            reader.onload = function (e) {
                $('#image-preview').attr('src', e.target.result).removeClass('d-none');
            }
            reader.readAsDataURL(this.files[0]);
        });

        function loadArticles(page = 1) {
            $.ajax({
                url: "/activities?page=" + page,
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    var rows = '';
                    $.each(data.data, function (key, article) {
                        let d = new Date(article.created_at);
                        let tanggal = d.toLocaleString();
                        rows += '<tr>';
                        rows += '<td>' + jenisBus(article.jenis) + '</td>';
                        rows += '<td>' + article.nama_cabang + '</td>';
                        rows += '<td>' + article.process + '</td>';
                        rows += '<td> <img src="' + ass + article.images + '" style="width: 50px" /></td>';
                        rows += '<td>' + article.name + '</td>';
                        rows += '<td>' + tanggal + '</td>';
                        rows += '<td>';
                        rows += '<button data-id="' + article.id + '" class="btn btn-warning edit-article">Edit</button> ';
                        rows += '<button data-id="' + article.id + '" class="btn btn-danger delete-article">Delete</button>';
                        rows += '</td>';
                        rows += '</tr>';
                    });
                    $('#articles-table tbody').html(rows);
                    $('#pagination').html(data.pagination);
                }
            });
        }

        $('body').on('click', '#create-new-article', function () {
            $('#save-btn').val("create-article");
            $('#activity-id').val('');
            $('#activity-id').val('');
            $('#image-preview').attr('src', '#').addClass('d-none');
            $('#article-form').trigger("reset");
            $('#article-modal-title').html("Tambah Kegiatan");
            $('#article-modal').modal('show');
        });
        $('body').on('click', '.edit-article', function () {
            var id = $(this).data('id');
            $.get('activities/' + id, function (data) {
                $('#article-modal-title').html("Ubah Kegiatan");
                $('#save-btn').val("edit-article");
                $('#article-modal').modal('show');
                $('#activity-id').val(data.id);
                $('#jenis').val(data.jenis);
                $('#id_bus').val(data.id_bus);
                $('#process').val(data.process);
            });
        });

        $('body').on('submit', '#article-form', function (e) {
            e.preventDefault();
            var actionType = $('#save-btn').val();
            var id = $('#activity-id').val();
            var method = actionType == "edit-article" ? "PUT" : "POST";
            var url = actionType == "edit-article" ? 'activities/' + id : 'activities';
            let formData = new FormData(this);

            $.ajax({
                type: method,
                url: url,
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    $('#article-form').trigger("reset");
                    $('#article-modal').modal('hide');
                    $('#success-message').html(data.message).removeClass('d-none');
                    loadArticles();
                }
            });
        });

        // $('body').on('submit', '#article-form', function (e) {
        //     e.preventDefault();
        //     var actionType = $('#save-btn').val();
        //     var id = $('#activity-id').val();
        //     var method = actionType == "edit-article" ? "PUT" : "POST";
        //     var url = actionType == "edit-article" ? 'activities/' + id : 'activities';
        //     let formData = new FormData(this);

        //     $.ajax({
        //         type: method,
        //         url: url,
        //         data: $('#article-form').serialize(),
        //         success: function (data) {
        //             $('#article-form').trigger("reset");
        //             $('#article-modal').modal('hide');
        //             $('#success-message').html(data.message).removeClass('d-none');
        //             loadArticles();
        //         }
        //     });
        // });

        $('body').on('click', '.delete-article', function () {
            var id = $(this).data('id');
            if (confirm("Are you sure want to delete this article?")) {
                $.ajax({
                    type: "DELETE",
                    url: 'activities/' + id,
                    success: function (data) {
                        $('#success-message').html(data.message).removeClass('d-none');
                        loadArticles();
                    }
                });
            }
        });

        $('body').on('click', '.pagination a', function (e) {
            e.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            loadArticles(page);
        });
    });
</script>
@endsection