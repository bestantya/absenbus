@extends('layouts.applogin')

@section('content')

<!-- /.login-logo -->
<div class="card card-outline card-primary">
    <div class="card-header text-center">
        <img src="{{ asset('img/logo.png') }}" alt="" srcset="" width="100px">
    </div>
    <div class="card-body">
        <p class="login-box-msg"><b>PT Riffa Mitra Teknik </b></p>

        <form action="{{ route('login') }}" method="post">
            @csrf
            <div class="input-group mb-3">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                    value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="{{ __('E-Mail') }}">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-envelope"></span>
                    </div>
                </div>
            </div>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            <div class="input-group mb-3">
                <input type="password" class="form-control @error('password') is-invalid @enderror" name="password"
                    required autocomplete="current-password" id="password"
                    placeholder="{{ __('Kata Sandi / Password') }}">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
            </div>
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            <div class="row">
                <div class="col-8">
                    <div class="icheck-primary">
                        <input type="checkbox" type="checkbox" name="remember" id="remember" {{ old('remember')
    ? 'checked' : '' }}>
                        <label for="remember">
                            {{ __('Ingat Saya') }}
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-4">
                    <button type="submit" class="btn btn-primary btn-block">{{ __('Masuk') }}</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
        <!-- /.social-auth-links -->

        <p class="mb-1">
            @if (Route::has('password.request'))
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Lupa kata sandi?') }}
                </a>
            @endif
        </p>
        <p class="mb-0">
            @if (Route::has('register'))
                <a class="btn btn-link" href="{{ route('register') }}">{{ __('Register') }}</a>
            @endif
        </p>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
</div>
<!-- /.login-box -->
@endsection