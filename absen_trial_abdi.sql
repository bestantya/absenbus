/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 8.0.17 : Database - trial_absen
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

/*Table structure for table `cabang` */

CREATE TABLE `cabang` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama_cabang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat_cabang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cabang` */

insert  into `cabang`(`id`,`nama_cabang`,`alamat_cabang`,`lat`,`lang`,`images`,`active`,`created_at`,`updated_at`) values 
(1,'Pools 1','Mana Saja','1','1','','1','2024-05-14 21:55:40','2024-05-14 21:55:42');

/*Table structure for table `failed_jobs` */

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `failed_jobs` */

/*Table structure for table `master_bus` */

CREATE TABLE `master_bus` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `jenis` tinyint(4) NOT NULL DEFAULT '0',
  `plat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `master_bus` */

insert  into `master_bus`(`id`,`jenis`,`plat`,`created_at`,`updated_at`) values 
(1,1,'K 21 L','2024-05-14 21:54:50','2024-05-14 21:54:52');

/*Table structure for table `migrations` */

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(7,'2024_05_12_142229_create_activity_table',2),
(9,'2014_10_12_000000_create_users_table',3),
(10,'2014_10_12_100000_create_password_resets_table',3),
(11,'2019_08_19_000000_create_failed_jobs_table',3),
(12,'2024_05_12_141512_create_master_bus_table',4),
(13,'2024_05_12_141941_create_cabang_table',4),
(14,'2024_05_14_140556_create_activities_table',4),
(15,'2024_05_14_161153_create_absensi_table',5);

/*Table structure for table `password_resets` */

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `users` */

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nik` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'users',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`nik`,`name`,`email`,`email_verified_at`,`password`,`roles`,`remember_token`,`created_at`,`updated_at`) values 
(1,'-','admin','admin@email.com',NULL,'$2y$10$FUZEPeawqGdAC/RYOhyKieCROvEkl3AKKbKU1r1G5i.XjcucDfEc6','users','4nMzBMhsij4NoZCYVIlNa4oU9tW3crBIoMtCtyHwtCF8k3t6KAdQQz9WVdEg','2024-05-14 14:31:51','2024-05-14 14:31:51');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


/*Table structure for table `absensi` */

CREATE TABLE `absensi` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `id_cabang` bigint(20) unsigned NOT NULL,
  `jenis` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'checkin',
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `long` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `images` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `absensi_user_id_foreign` (`user_id`),
  KEY `absensi_id_cabang_foreign` (`id_cabang`),
  CONSTRAINT `absensi_id_cabang_foreign` FOREIGN KEY (`id_cabang`) REFERENCES `cabang` (`id`),
  CONSTRAINT `absensi_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `absensi` */

insert  into `absensi`(`id`,`user_id`,`id_cabang`,`jenis`,`lat`,`long`,`images`,`created_at`,`updated_at`) values 
(1,1,1,'checkin','1','1','','2024-05-14 21:56:23',NULL);

/*Table structure for table `activities` */

CREATE TABLE `activities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `jenis` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'cleaning_bus',
  `id_bus` bigint(20) unsigned NOT NULL,
  `id_cabang` bigint(20) unsigned NOT NULL,
  `process` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `images` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `activities_id_bus_foreign` (`id_bus`),
  KEY `activities_id_cabang_foreign` (`id_cabang`),
  CONSTRAINT `activities_id_bus_foreign` FOREIGN KEY (`id_bus`) REFERENCES `master_bus` (`id`),
  CONSTRAINT `activities_id_cabang_foreign` FOREIGN KEY (`id_cabang`) REFERENCES `cabang` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `activities` */

insert  into `activities`(`id`,`jenis`,`id_bus`,`id_cabang`,`process`,`images`,`created_at`,`updated_at`) values 
(1,'cleaning_bus',1,1,'','','2024-05-14 15:28:00','2024-05-14 15:28:00'),
(2,'cleaning_bus',1,1,'berlangsung','','2024-05-14 15:53:45','2024-05-14 15:53:45'),
(3,'cleaning_bus',1,1,'setelah','','2024-05-14 15:54:39','2024-05-14 15:54:39');
