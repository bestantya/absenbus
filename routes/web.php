<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ActivityController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('activities', 'ActivityController@index')->name('activity.index');
// Route::get('activities', [ActivityController::class, 'index'])->name('activities.index');
Route::post('activities', 'ActivityController@store')->name('activity.store');
Route::get('activities/{id}', 'ActivityController@show')->name('activity.show');
Route::put('activities/{id}', 'ActivityController@update')->name('activity.update');
Route::delete('activities/{id}', 'ActivityController@destroy')->name('activity.destroy');
Route::get('ajax/getlistbus', 'ActivityController@getBus')->name('activity.getBus');