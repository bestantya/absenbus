<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable = ['jenis', 'id_bus', 'id_cabang', 'process', 'images', 'id_user'];
}
