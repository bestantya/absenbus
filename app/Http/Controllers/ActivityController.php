<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Activity;
use App\Master_bus;
use Illuminate\Support\Facades\DB;
use Auth;

class ActivityController extends Controller
{
    public function index(Request $request)
    {
        // $Activities = Activity::paginate(5);
        $Activities = DB::table('activities')
            ->leftJoin('master_buses', 'master_buses.id', '=', 'activities.id')
            ->leftJoin('master_cabangs', 'master_cabangs.id', '=', 'activities.id_cabang')
            ->leftJoin('users', 'users.id', '=', 'activities.id_user')
            ->select('activities.*', 'master_buses.jenis', 'master_buses.plat', 'master_cabangs.nama_cabang', 'users.name')
            ->paginate(5);
        $bus = DB::table('master_buses')->get();
        $cabang = DB::table('master_cabangs')->get();
        // dd($bus);
        if ($request->ajax()) {
            return response()->json([
                'data' => $Activities->items(),
                'pagination' => (string) $Activities->links()
            ]);
        }
        $dt = [
            'bus' => $bus,
            'cabang' => $cabang,
        ];
        return view('activity.index', $dt);
    }
    public function store(Request $request)
    {
        $request->validate([
            'id_bus' => 'required',
            'process' => 'required',
        ]);
        $input = $request->all();
        if ($request->hasFile('images')) {
            $input['images'] = time() . '.' . $request->images->extension();
            $request->images->move(public_path('img'), $input['images']);
            // AjaxImage::create($input);

        }
        $input['id_user'] = Auth::id();
        $Activity = Activity::create($input);
        return response()->json(['message' => 'Data berhasil disimpan.']);
    }

    public function show($id)
    {
        $Activity = Activity::find($id);
        return response()->json($Activity);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'id_bus' => 'required',
            'process' => 'required',
        ]);

        $Activity = Activity::find($id);
        $Activity->update($request->all());
        return response()->json(['message' => 'Data berhasil diubah.']);
    }

    public function destroy($id)
    {
        Activity::find($id)->delete();
        return response()->json(['message' => 'Data berhasil dihapus.']);
    }

    public function getBus(Request $request)
    {
        $search = $request->get('term');
        $result = Master_bus::where('plat', 'LIKE', '%' . $search . '%')->get();
        return response()->json($result);
    }
}